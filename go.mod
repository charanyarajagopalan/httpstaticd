module gitlab.com/charanyarajagopalan/httpstaticd

go 1.16

require (
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/sirupsen/logrus v1.8.0
	github.com/urfave/cli v1.22.5
)
