FROM golang as builder
COPY . /go/src/gitlab.com/charanyarajagopalan/httpstaticd
WORKDIR /go/src/gitlab.com/charanyarajagopalan/httpstaticd
RUN go get ./... && \
    CGO_ENABLED=0 GOOS=linux go build -o httpstaticd cmd/httpstaticd/httpstaticd.go

FROM scratch
COPY --from=builder /go/src/gitlab.com/charanyarajagopalan/httpstaticd/httpstaticd /httpstaticd
WORKDIR /
ENTRYPOINT ["./httpstaticd"]
